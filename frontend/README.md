Toolbox Application Front-End
=============================

This is a small React package for the ` Toolbox Application` test.

![](https://img.shields.io/badge/node-success-brightgreen.svg)
![](https://img.shields.io/badge/test-success-brightgreen.svg)

# Stack

![](https://img.shields.io/badge/react-✓-blue.svg)
![](https://img.shields.io/badge/redux-✓-blue.svg)
![](https://img.shields.io/badge/jest-✓-blue.svg)
![](https://img.shields.io/badge/karma-✓-blue.svg)
![](https://img.shields.io/badge/ES6-✓-blue.svg)

# File structure

```
frontend/
│
├── src/
│   ├── components/
│   │   ├── App/
│   │   │   ├── index.js
│   │   │   ├── App.scss
│   │   │   ├── App.test.js
│   │   │   └── App.jsx
│   |   |
│   │   ├── Form/
│   │   │   ├── index.js
│   │   │   ├── Form.scss
│   │   │   ├── Form.test.js
│   │   │   └── Form.jsx
│   │   │
│   │   └── List/
│   │       ├── index.js
│   │       ├── List.scss
│   │       ├── List.test.js
│   │       └── List.jsx
│   │
│   ├── store/
│   │   ├── app/
│   │   │   ├── app.sagas.js
│   │   │   ├── app.selectors.js
│   │   │   └── app.state.js
│   │   │
│   │   ├── sagas.js
│   │   └── index.js
│   │
│   ├── effects/
│   │   ├── message.js
│   │   └── client.js
│   │
│   ├── index.js
│   ├── index.scss
│   ├── setupTests.js
│   └── routes.js
│
├── .env                          * Environment configuration file
├── .gitignore                    * Example git ignore file
├── package.json                  * Defines our JavaScript dependencies
├── yarn.lock                     * Defines our exact JavaScript dependencies tree
└── README.md                     * This file
```

## How to use this code?

1. Make sure you have the latest stable version of Node.js installed

  ```
  $ sudo npm cache clean -f
  $ sudo npm install -g n
  $ sudo n stable
  ```

2. Fork this repository and clone it

  ```
  $ git clone https://bitbucket.org/rodtron/toolbox-application
  ```

3. Navigate into the front-end folder

  ```
  $ cd frontend
  ```

4. Install NPM dependencies

  ```
  $ yarn install
  ```

5. Use `yarn start` to run the project with `react-scripts`

  ```
  $ yarn start
  ```

7. If you want to execute the tests

  ```
  $ yarn test
  ```
