import { createActions } from 'redux-arc';

export const { creators, types } = createActions('messages', {
  list: { url: 'messages', method: 'get' },
  create: { url: 'messages', method: 'post' },
  deleteAll: { url: 'messages', method: 'delete' },
  delete: { url: 'messages/:id', method: 'delete' },
});
