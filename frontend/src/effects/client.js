import axios from 'axios';

const axiosInstance = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
  headers: {
    'Content-Type': 'application/json',
  }
});

export const asyncTask = store => done => (options) => {
  const { method, url, payload } = options;
  const params = method === 'get' ? { params: payload } : payload;
  axiosInstance[method](url, params, options).then(
    response => done(null, response.data),
    error => done(error, null)
  );
};

export default axiosInstance;
