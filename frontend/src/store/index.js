import { connectRouter, routerMiddleware } from 'connected-react-router';
import { createBrowserHistory } from 'history';

import { applyMiddleware, createStore, combineReducers, compose } from 'redux';
import { createAsyncMiddleware } from 'redux-arc';
import createSagaMiddleware from 'redux-saga';

import App from './app/app.state';
import rootSaga from './sagas';
import { asyncTask } from '../effects/client';

export const sagaMiddleware = createSagaMiddleware();
export const history = createBrowserHistory();

const reducer = combineReducers({
  router: connectRouter(history),
  App,
});

const asyncMiddleware = createAsyncMiddleware(asyncTask);

const enhancers = compose(
  applyMiddleware(
    routerMiddleware(history),
    asyncMiddleware,
    sagaMiddleware,
  )
);

export default createStore(reducer, enhancers);

// run saga engine
sagaMiddleware.run(rootSaga);