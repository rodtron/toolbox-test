import { all, fork, put, take } from 'redux-saga/effects';
import { creators, types } from '../../effects/message';

export function* bootstrap() {
  yield put(creators.list());
}

export function* watchRemoveAllResponse() {
  while (true) {
    const action = yield take(types.DELETE_ALL.RESPONSE);

    if (!action.error) {
      yield put(creators.list());
    }
  }
}

export default function* appSagas() {
  yield all([
    fork(bootstrap),
    fork(watchRemoveAllResponse),
  ]);
}
