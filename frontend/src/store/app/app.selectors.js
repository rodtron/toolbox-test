import { createSelector } from 'reselect';

export const appState = state => state.App;

export const selectMessages = createSelector(
  appState,
  state => state.messages.reverse()
);

export const selectResponse = createSelector(
  appState,
  state => state.response
);

export const selectErrors = createSelector(
  appState,
  state => state.errors
);

export const selectLoaders = createSelector(
  appState,
  state => state.loading
);
