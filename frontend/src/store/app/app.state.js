import { createReducers } from 'redux-arc';
import { types } from '../../effects/message';

export const initialState = {
  errors: {
    form: null,
    list: null,
    create: null,
    delete: null,
  },
  loading: {
    list: false,
    create: false,
    delete: false,
  },
  messages: [],
  response: '',
};

const onRequest = type => (state, action) => ({
  ...state,
  errors: {
    ...state.errors,
    [type]: initialState.errors[type],
  },
  loading: {
    ...state.loading,
    [type]: true,
  }
});

const applyErrors = (type, state, action) => ({
  ...state,
  errors: {
    ...state.errors,
    [type]: action.payload.message || action.payload.error,
  },
  loading: {
    ...state.loading,
    [type]: false,
  }
});

const onListResponse = (state, action) => {
  if (action.error) {
    return applyErrors('list', state, action)
  }

  return {
    ...state,
    loading: { ...state.loading, list: false },
    errors: { ...state.errors, list: initialState.errors.list },
    messages: action.payload,
  }
};

const onCreateResponse = (state, action) => {
  if (action.error) {
    return applyErrors('create', state, action)
  }

  return {
    ...state,
    loading: { ...state.loading, create: false },
    errors: { ...state.errors, create: initialState.errors.create },
    messages: [...state.messages, action.payload],
    response: action.payload.title,
  }
};

const onDeleteResponse = (state, action) => {
  if (action.error) {
    return applyErrors('delete', state, action)
  }

  const index = state.messages.findIndex(
    message => message.id === action.payload.id
  );

  return {
    ...state,
    loading: { ...state.loading, delete: false },
    errors: { ...state.errors, delete: initialState.errors.delete },
    messages: [
      ...state.messages.slice(0, index),
      ...state.messages.slice(index + 1),
    ]
  }
};

const HANDLERS = {
  [types.LIST.REQUEST]: onRequest('list'),
  [types.CREATE.REQUEST]: onRequest('create'),
  [types.DELETE.REQUEST]: onRequest('delete'),
  [types.LIST.RESPONSE]: onListResponse,
  [types.CREATE.RESPONSE]: onCreateResponse,
  [types.DELETE.RESPONSE]: onDeleteResponse,
};

export default createReducers(initialState, HANDLERS);
