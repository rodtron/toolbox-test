import {
  selectErrors, selectLoaders,
  selectMessages,
  selectResponse
} from '../app.selectors';

describe('app selectors', () => {
  it('should select messages', () => {
    const mock = {
      messages: [1, 2, 3]
    };
    const selected = selectMessages.resultFunc(mock);
    expect(selected).toEqual(mock.messages.reverse());
  });
  it('should select response', () => {
    const mock = {
      response: 'test'
    };
    const selected = selectResponse.resultFunc(mock);
    expect(selected).toEqual(mock.response);
  });
  it('should select errors', () => {
    const mock = {
      errors: {
        list: true,
        create: false
      }
    };
    const selected = selectErrors.resultFunc(mock);
    expect(selected.list).toEqual(true);
    expect(selected.create).toEqual(false);
  });
  it('should select loaders', () => {
    const mock = {
      loading: {
        list: false,
        create: true
      }
    };
    const selected = selectLoaders.resultFunc(mock);
    expect(selected.list).toEqual(false);
    expect(selected.create).toEqual(true);
  });
});
