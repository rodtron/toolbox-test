import { expect } from 'chai';
import reducer, { initialState } from '../app.state';
import { types } from '../../../effects/message';

describe('app reducer', () => {
  it('returns the initial state', () => {
    expect(reducer(undefined, {})).to.deep.equal(initialState);
  });

  it('handles list request', () => {
    expect(reducer(initialState, { type: types.LIST.REQUEST })).to.deep.equal({
      ...initialState,
      loading: {
        ...initialState.loading,
        list: true
      },
    });
  });

  it('handles list response', () => {
    expect(reducer(initialState, {
      type: types.LIST.RESPONSE,
      payload: [{ title: 'dummy' }]
    })).to.deep.equal({
      ...initialState,
      messages: [{ title: 'dummy' }],
    });
  });

  it('handles create request', () => {
    expect(reducer(initialState, { type: types.CREATE.REQUEST })).to.deep.equal({
      ...initialState,
      loading: {
        ...initialState.loading,
        create: true
      },
    });
  });

  it('handles create response', () => {
    expect(reducer(initialState, {
      type: types.CREATE.RESPONSE,
      payload: { title: 'dummy' }
    })).to.deep.equal({
      ...initialState,
      messages: [{ title: 'dummy' }],
      response: 'dummy'
    });
  });

  it('handles delete request', () => {
    expect(reducer(initialState, { type: types.DELETE.REQUEST })).to.deep.equal({
      ...initialState,
      loading: {
        ...initialState.loading,
        delete: true
      },
    });
  });

  it('handles delete response', () => {
    const state = {
      ...initialState,
      messages: [{ id: 1 }]
    };
    expect(reducer(state, {
      type: types.DELETE.RESPONSE,
      payload: { id: 1 }
    })).to.deep.equal(initialState);
  });
});
