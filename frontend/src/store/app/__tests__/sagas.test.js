import { expectSaga } from 'redux-saga-test-plan';
import { creators, types } from '../../../effects/message';
import { bootstrap, watchRemoveAllResponse } from '../app.sagas';

describe('app sagas', () => {
  it('fetches list of messages on application bootstrap', () => {
    return expectSaga(bootstrap)
      .put(creators.list())
      .run();
  });
  it('should fetch the list of messages when deleting all messages succeeds', () => {
    return expectSaga(watchRemoveAllResponse)
      .take(types.DELETE_ALL.RESPONSE)
      .put(creators.list())
      .dispatch({ type: types.DELETE_ALL.RESPONSE })
      .run();
  });
});
