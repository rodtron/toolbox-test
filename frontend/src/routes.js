import { ConnectedRouter } from 'connected-react-router';

import React from 'react';
import { Route, Switch } from 'react-router';

import App from './components/App';
import { history } from './store';

export const HOME_PATH = '/';

export const renderOutlet = (routes) => (
  <ConnectedRouter history={history}>
    <Switch>
      { routes.map((route, index) => (
        <Route
          key={index}
          {...route}
        />
      )) }
    </Switch>
  </ConnectedRouter>
);

export const routes = [
  { path: HOME_PATH,
    exact: true,
    component: App
  }
];