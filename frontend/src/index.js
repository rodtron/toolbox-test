import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import 'bootstrap/dist/css/bootstrap.css';
import './índex.css';

import Store from './store';
import { renderOutlet, routes } from './routes';

export function renderApp() {
  render(
    <Provider store={Store}>
      { renderOutlet(routes) }
    </Provider>,
    document.getElementById('root')
  );
}

renderApp();
