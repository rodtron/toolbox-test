import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import List from './List';

const mockMessages = [
  { title: 'Message 1' },
  { title: 'Message 2' },
  { title: 'Message 3' },
];

const list = <List messages={mockMessages} />;

// smoke test
it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(list, div);
  ReactDOM.unmountComponentAtNode(div);
});

describe('Message List', () => {
  it('should not render component when no messages', () => {
    const wrapper = shallow(<List messages={[]} />);
    expect(wrapper.exists()).to.be.true;
  });

  it('should render messages', () => {
    const wrapper = shallow(list);
    expect(wrapper.find('li')).to.have.lengthOf(mockMessages.length);
  });
});
