import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { creators } from '../../effects/message';
import { selectMessages } from '../../store/app/app.selectors';
import List from './List.jsx';

List.propTypes = {
  messages: PropTypes.arrayOf(PropTypes.object).isRequired,
};

const mapStateToProps = state => ({
  messages: selectMessages(state),
});

const mapDispatchToProps = dispatch => ({
  refresh: () => dispatch(creators.list()),
  removeAll: () => dispatch(creators.deleteAll()),
});

export default connect(mapStateToProps, mapDispatchToProps)(List);
