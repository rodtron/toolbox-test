import React from 'react';
import './List.css';

const List = props => {
  return props.messages.length !== 0 && (
    <div className="list">
      <span className="header">
        All Messages
        <button className="btn btn-link" onClick={props.refresh}>Refresh</button> |
        <button className="btn btn-link" onClick={props.removeAll}>Remove All</button>
      </span>
      <ol>
        { props.messages.map((v, i) => v.title && (<li key={i}>{v.title}</li>)) }
      </ol>
    </div>
  );
};

export default List;
