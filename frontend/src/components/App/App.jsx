import React from 'react';
import './App.css';

import Form from '../Form';
import List from '../List'

const App = () => {
  return (
    <div className="App">
      <Form className="form" />
      <List className="list" />
    </div>
  );
};

export default App;
