import React from 'react';
import './Form.css';

const Form = props => {
  const handleSubmit = event => {
    const form = event.target;
    event.preventDefault();

    if (!form.checkValidity()) {
      form.classList.add('was-validated');
      return;
    }

    const data = {};
    for (let element of form.elements) {
      if (element.tagName === 'BUTTON') { continue; }
      data[element.name] = element.value;
    }

    form.classList.remove('was-validated');
    return props.submit(data);
  };

  return (
    <form className="form" onSubmit={handleSubmit} noValidate>
      <label htmlFor="message">Please enter your message</label>
      <div className="input-group">
        <input
          type="text"
          id="message"
          name="message"
          placeholder="Message..."
          className="form-control"
          disabled={props.loading.create}
          maxLength="20"
          minLength="3"
          required
        />
        <div className="input-group-append">
          <button
            type="submit"
            className="btn btn-outline-secondary"
            disabled={props.loading.create}
          >Send</button>
        </div>
      </div>
      { props.errors.create && (
        <div className="errors">
          { props.errors.create }
        </div>
      ) }
      { props.response && (
        <div className="latest-message">
          Response from server: <strong>{ props.response }</strong>
        </div>
      ) }
    </form>
  );
};

export default Form;
