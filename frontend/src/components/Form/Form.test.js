import { expect } from 'chai';
import { mount } from 'enzyme';
import sinon from 'sinon';

import React from 'react';
import ReactDOM from 'react-dom';
import Form from './Form';

const form = props => <Form loading={{}} errors={{}} {...props} />;

// smoke test
it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(form(), div);
  ReactDOM.unmountComponentAtNode(div);
});


describe('Form', () => {
  const submit = sinon.fake();
  const wrapper = mount(form({ submit }));

  it('should display response from server dialog', () => {
    const response = 'Test Response';
    const responseWrapper = mount(form({ response }));
    const el = responseWrapper.find('.latest-message');
    expect(el.exists()).to.be.true;
    expect(el.text()).to.match(/Test Response/);
  });

  it('should not submit when form is invalid', () => {
    const el = wrapper.find('form');
    el.simulate('submit', { preventDefault: jest.fn() });
    expect(submit.called).to.be.false;
  });

  it('should dispatch submit action if form is valid', () => {
    const el = wrapper.find('form');
    const input = wrapper.find('input');
    input.getDOMNode().value = 'Testing Dispatch';
    el.simulate('submit', { preventDefault: jest.fn() });
    expect(submit.calledWith({ message: 'Testing Dispatch' })).to.be.true;
  });
});
