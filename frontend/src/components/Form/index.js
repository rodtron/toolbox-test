import Form from './Form.jsx';

import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';

import { creators } from '../../effects/message';
import {
  selectErrors,
  selectLoaders,
  selectResponse
} from '../../store/app/app.selectors';

Form.propTypes = {
  response: PropTypes.string,
  loading: PropTypes.object,
};

const mapStateToProps = state => ({
  errors: selectErrors(state),
  response: selectResponse(state),
  loading: selectLoaders(state),
});

const mapDispatchToProps = dispatch => ({
  submit: payload => dispatch(creators.create(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Form);