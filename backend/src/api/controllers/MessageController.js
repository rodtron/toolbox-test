import Joi from 'joi';
import Message from '../models/Message';
import Schema from '../../config/routes/validation/message';

function create(req, res) {
  Joi.validate(req.body, Schema.create, (err) => {
    if (err) {
      return res.status(422).json({ error: 'Invalid request data' });
    }

    const data = { title: req.body.message.trim() };
    return Message.create(data)
      .then(async (newMessage) => {
        newMessage = await Message.findById(newMessage.id);
        res.status(201).json(newMessage);
      }).catch((e) => {
        res.status(500).json({ error: e.message });
      });
  });
}

function list(req, res) {
  const { offset = 0, limit = 50 } = req.query;
  Message.find().limit(limit).skip(offset)
    .then((messages) => {
      res.status(200).json(messages);
    })
    .catch((e) => {
      res.status(500).json({ error: e.message });
    });
}

function removeAll(req, res) {
  Message.remove()
    .then(() => {
      res.status(204).json();
    }).catch((e) => {
      res.status(500).json({ error: e.message });
    });
}

export default { create, list, removeAll };
