import mongoose from 'mongoose';

const Schema = mongoose.Schema;

// Define collection and schema for Message
const Message = new Schema({
  title: {
    type: String,
  },
}, {
  collection: 'messages',
});

export default mongoose.model('Message', Message);
