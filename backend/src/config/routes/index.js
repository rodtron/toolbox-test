import express from 'express';
import messageRoutes from './message';

const router = express.Router();

router.get('/status', (req, res) => res.json({ status: "ok" }));

router.use('/messages', messageRoutes);

module.exports = router;
