import { Router } from 'express';
import messageCtrl from '../../api/controllers/MessageController';

const router = Router();

router.route('/')
  .get(messageCtrl.list)
  .post(messageCtrl.create)
  .delete(messageCtrl.removeAll);

export default router;
