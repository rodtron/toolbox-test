import Joi from 'joi';

export default {
  create: {
    message: Joi.string().min(3).max(20)
      .required(),
  },
};
