import 'babel-polyfill';
import bodyParser from 'body-parser';
import express from 'express';
import mongoose from 'mongoose';
import dotenv from 'dotenv';
import routes from './config/routes';

dotenv.config();
const PORT = process.env.PORT || 9000;
const app = express();

mongoose.connect(process.env.MONGODB_URI, { useNewUrlParser: true }).then(
  () => { console.log('Database is connected'); },
  (err) => { console.log(`Can not connect to the database${err}`); },
);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  res.header('Access-Control-Allow-Methods', 'POST, PUT, GET, DELETE, OPTIONS');
  next();
});

// Mount all routes on /api path
app.use('/api', routes);

app.listen(PORT, () => console.log(`Server started on port ${PORT}`));

export default app;
