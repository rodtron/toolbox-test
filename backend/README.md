Toolbox Application API
=======================

This is a small API package that backs the ` Toolbox Application` test.

![](https://img.shields.io/badge/node-success-brightgreen.svg)
![](https://img.shields.io/badge/test-success-brightgreen.svg)

# Stack

![](https://img.shields.io/badge/node_8-✓-blue.svg)
![](https://img.shields.io/badge/ES6-✓-blue.svg)
![](https://img.shields.io/badge/express-✓-blue.svg)
![](https://img.shields.io/badge/mocha-✓-blue.svg)

# File structure

```
backend/
│
├── src/
│   │
│   ├── api/
│   │   ├── controllers/
│   │   │   └── MessageController.js
│   │   │
│   │   └── models/
│   │       └── Message.js
│   │
│   ├── config/
│   │   ├── routes/
│   │   │   ├── validation/
│   │   │   │   └── message.js
│   │   │   │
│   │   │   ├── index.js
│   │   │   └── message.js
│   │   │
│   │   └── express.js
│   │
│   └── server.js
│
├── test/
│   └── message.test.js
│
├── .env.example                  * Example environment configuration file
├── .eslintrc                     * ESLint configuration file
├── .gitignore                    * Example git ignore file
├── start.js                      * Entry point of our Node's app
├── package.json                  * Defines our JavaScript dependencies
├── yarn.lock                     * Defines our exact JavaScript dependencies tree
└── README.md                     * This file
```

## How to use this code?

1. Make sure you have the latest stable version of Node.js installed

  ```
  $ sudo npm cache clean -f
  $ sudo npm install -g n
  $ sudo n stable
  ```

2. Fork this repository and clone it

  ```
  $ git clone https://bitbucket.org/rodtron/toolbox-application
  ```

3. Navigate into the folder

  ```
  $ cd api
  ```

4. Configure the application using `.env` file (copy from `.env.example`)

  ```
  MONGODB_URI=mongodb://localhost:27017/posts
  ```

5. Install NPM dependencies

  ```
  $ yarn install
  ```

6. Use `nodemon` for running the project with live-reload

  ```
  $ yarn start
  ```

  > `yarn start` will run `nodemon index.js`.

7. Navigate to `http://localhost:8000/api/status` in your browser to check you're seing the following response

  ```javascript
  { "status": "ok" }
  ```

  > The port can be changed by the setting the environment variable `PORT`

8. If you want to execute the tests

  ```
  $ yarn test
  ```
