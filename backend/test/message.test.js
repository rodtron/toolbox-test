process.env.NODE_ENV = 'test';

import request from 'supertest-as-promised';
import { assert, expect } from 'chai';
import app from '../src/server';
import Message from '../src/api/models/Message';

describe('Message', () => {
  beforeEach(async () => {
    await Message.deleteMany();
    await Message.create({
      title: 'Test Message',
    });
    await Message.create({
      title: 'Second Test Message',
    });
  });

  describe('GET /api/messages', () => {
    it('It should GET all the messages', (done) => {
      request(app)
        .get('/api/messages')
        .expect(200)
        .then((res) => {
          expect(res.body).to.be.an.instanceof(Array);
          expect(res.body.length).equal(2);
          done();
        });
    });
  });

  describe('POST /api/messages', () => {
    it('It should create a new message', (done) => {
      request(app)
        .post('/api/messages')
        .send({
          message: 'Another Test Message',
        })
        .expect(201)
        .then((res) => {
          expect(res.body.title).equal('Another Test Message');
          done();
        });
    });
    it('It should fail to create a new message', (done) => {
      request(app)
        .post('/api/messages')
        .send({
          message: '',
        })
        .expect(422, done());
    })
  });

  describe('DELETE /api/messages/', () => {
    it('It should delete all messages', (done) => {
      request(app)
        .delete('/api/messages')
        .expect(204, done());
    });
  });

});
