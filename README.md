Toolbox Application
===================

This is a small package for the `Toolbox Application` test.

## Getting Started

You can use `docker-compose` to run both the API and Front-end by using the following commmand

```
$ docker-compose build
$ docker-compose up
```

Then you can visit `http://localhost:3000` in your browser to view the site.

To run each service as a stand-alone process please read the `README.md` files inside `api` and `frontend` folders.
